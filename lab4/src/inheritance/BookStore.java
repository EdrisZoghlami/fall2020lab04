//Edris Zoghlami
//1935687

package inheritance;

public class BookStore {

	public static void main(String[] args) {
		Book books[] =new Book[5];
		books[0]=  new Book("author1", "book1");
		books[1]= new ElectronicBook("author2", "book2", 500);
		books[2]= new Book("author3", "book3");
		books[3]= new ElectronicBook("author4", "book4", 100);
		books[4]= new ElectronicBook("author5", "book5", 750);
		
		for(int i=0;i<5;i++) {
			
			if(books[i] instanceof ElectronicBook) {
				
				System.out.println(books[i].toString()); 
			}
			else {
				System.out.println(books[i].toString()); 
			}
		

		}

	}
}
