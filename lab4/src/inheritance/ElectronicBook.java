//Edris Zoghlami
//1935687


package inheritance;

public class ElectronicBook extends Book{
	int numberBytes;
	
	public ElectronicBook(String title, String author, int numberBytes) {
		
		super(title, author);
		this.numberBytes=numberBytes;	
	}
	
	
	public String toString() {
		
		return super.toString()+" "+numberBytes;
	}
	
	
	
}
