//Edris Zoghlami
//1935687


package geometry;

public interface Shape {
	
	double getArea();
	double getPerimeter();

}
