//Edris Zoghlami
//1935687



package geometry;

public class LotsOfShapes {

	public static void main(String[] args) {
		Shape shapes[]=new Shape[5];
		
		shapes[0]=new Rectangle(10.0,3.0);
		shapes[1]=new Rectangle(5.0,7.0);
		shapes[2]=new Circle(3);
		shapes[3]=new Circle(4);
		shapes[4]=new Square(5.0);
		
		for(int i=0;i<5;i++) {
			
		System.out.println(shapes[i].getArea()+" "+shapes[i].getPerimeter());
			}

	}
	

}
